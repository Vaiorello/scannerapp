from django.shortcuts import render, redirect
from .models import Liceu, Copil, Plati
from rest_framework import viewsets
from .serializers import LiceuSerializer, CopilSerializer, PlatiSerializer
import cv2, pytesseract, imutils, argparse
from skimage.filters import threshold_local
from skimage import io
from .forms import CopilForm
import numpy as np

def index(request):
    return render(request, 'scannerapp/index.html')

class LiceuView(viewsets.ModelViewSet):
    def get_queryset(self):
        result = Liceu.objects.all()
        return result

    def retrieve(self, request, pk=None):
        Liceu = get_object_or_404(Liceu, pk=pk)
        serializer = LiceuSerializer(Liceu)
        return Response(serializer.data)

    serializer_class = LiceuSerializer

class CopilView(viewsets.ModelViewSet):
    def get_queryset(self):
        result = Copil.objects.all()
        return result

    def retrieve(self, request, pk=None):
        Copil = get_object_or_404(Copil, pk=pk)
        serializer = CopilSerializer(Copil)
        return Response(serializer.data)

    serializer_class = CopilSerializer

class PlatiView(viewsets.ModelViewSet):
    def get_queryset(self):
        result = Plati.objects.all()
        return result

    def retrieve(self, request, pk=None):
        Plati = get_object_or_404(Plati, pk=pk)
        serializer = PlatiSerializer(Plati)
        return Response(serializer.data)

    serializer_class = PlatiSerializer

def order_points(pts):
    # initialzie a list of coordinates that will be ordered
    # such that the first entry in the list is the top-left,
    # the second entry is the top-right, the third is the
    # bottom-right, and the fourth is the bottom-left
    rect = np.zeros((4, 2), dtype = "float32")

    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis = 1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis = 1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    # return the ordered coordinates
    return rect

def four_point_transform(image, pts):
    # obtain a consistent order of the points and unpack them
    # individually
    rect = order_points(pts)
    (tl, tr, br, bl) = rect

    # compute the width of the new image, which will be the
    # maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

    # compute the height of the new image, which will be the
    # maximum distance between the top-right and bottom-right
    # y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))

    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype = "float32")

    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

    # return the warped image
    return warped

def detect(image):
    ratio = image.shape[0] / 500.0
    orig = image.copy()
    image = imutils.resize(image, height = 500)

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (5, 5), 0)
    edged = cv2.Canny(gray, 75, 200)
    
    cnts = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    cnts = sorted(cnts, key = cv2.contourArea, reverse = True)[:5]

    for c in cnts:
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)

        if len(approx) == 4:
            screenCnt = approx
            break

    warped = four_point_transform(orig, screenCnt.reshape(4, 2) * ratio)

    warped = cv2.cvtColor(warped, cv2.COLOR_BGR2GRAY)
    T = threshold_local(warped, 31, offset = 20, method = "gaussian")
    warped = (warped > T).astype("uint8") * 255

    warped = cv2.resize(warped, (int(1.41 * 650), 650))

    crop_img = warped[490:625, 25:900]

    return crop_img

def rotate_bound(image, angle):
    # grab the dimensions of the image and then determine the
    # center
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)
    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])
    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))
    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY
    # perform the actual rotation and return the image
    return cv2.warpAffine(image, M, (nW, nH))

def get_sex(cod):
    sex = cod[11]
    an = cod[4:6]

    if sex == 'M' and int(an) > 50:
        return 1
    elif sex == 'F' and int(an) > 50:
        return 2
    elif sex == 'M' and int(an) < 50:
        return 5
    elif sex == 'F' and int(an) < 50:
        return 6

def mobile(request):

    if request.method == 'POST':

        data = {'nume': '', 'prenume': '', 'serie': '', 'cnp': ''}

        try:

            image = cv2.imdecode(np.fromstring(request.FILES.get('poza').read(), np.uint8), cv2.IMREAD_UNCHANGED)

            image = rotate_bound(image, 90)

            text = pytesseract.image_to_string(detect(image))

            text = text.split('\n')
            line_1 = text[0]
            line_2 = text[1]

            line_1 = line_1.split('<<')
            line_1 = list(filter(lambda x: len(x) > 2, line_1))

            line_2 = line_2.split('<')

            data['nume'] = line_1[0][5:]
            data['prenume'] = line_1[1].replace('<', ' ')
            data['serie'] = line_2[0]
            cnp = line_2[1]
            data['cnp'] = f'{get_sex(cnp)}{cnp[4:10]}{cnp[-7:-1]}'
        
        except Exception as e:
            print(e)

        return redirect('scannerapp-add', nume = data['nume'], prenume = data['prenume'], serie = data['serie'], cnp = data['cnp'])

    return render(request, 'scannerapp/mobile.html')

def add(request, nume = '', prenume = '', serie = '', cnp = ''):

    form = CopilForm(initial={'nume': nume, 'prenume': prenume, 'serie_buletin': serie, 'cnp': cnp})

    return render(request, 'scannerapp/add.html', {'form': form})