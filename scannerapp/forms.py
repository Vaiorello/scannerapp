from django import forms
from .models import Liceu


class CopilForm(forms.Form):
    nume = forms.CharField(
        label = 'Nume',
        widget = forms.TextInput(
            attrs = {'class': 'form-control'}
        )
    )
    prenume = forms.CharField(
        label = 'Prenume',
        widget = forms.TextInput(
            attrs = {'class': 'form-control'}
        )
    )
    cnp = forms.CharField(
        label = 'CNP',
        widget = forms.TextInput(
            attrs = {'class': 'form-control'}
        )
    )
    telefon = forms.CharField(
        label = 'Telefon',
        widget = forms.TextInput(
            attrs = {'class': 'form-control'}
        )
    )
    serie_buletin = forms.CharField(
        label = 'Serie Buletin',
        widget = forms.TextInput(
            attrs = {'class': 'form-control'}
        )
    )
    liceu = forms.ModelChoiceField(Liceu.objects.all())