from rest_framework import serializers
from .models import Liceu, Plati, Copil

class LiceuSerializer(serializers.ModelSerializer):
    class Meta:
        model = Liceu
        fields = '__all__'

class CopilSerializer(serializers.ModelSerializer):
    liceu = LiceuSerializer(read_only = True)
    class Meta:
        model = Copil
        fields = '__all__'

class PlatiSerializer(serializers.ModelSerializer):
    copil = CopilSerializer(read_only = True)
    class Meta:
        model = Plati
        fields = '__all__'