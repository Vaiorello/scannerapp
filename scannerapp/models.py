from django.db import models

class Liceu(models.Model):
    denumire = models.CharField(max_length = 255)

    class Meta:
        verbose_name = 'Liceu'
        verbose_name_plural = 'Licee'

    def __str__(self):
        return self.denumire

class Copil(models.Model):
    nume = models.CharField(max_length = 150, blank = True)
    prenume = models.CharField(max_length = 255, blank = True)
    cnp = models.CharField(max_length = 30, blank = True)
    telefon = models.CharField(max_length = 50, blank = True)
    serie_buletin = models.CharField(max_length = 50, blank = True)
    liceu = models.ForeignKey(Liceu, on_delete = models.SET_NULL, null = True)

    class Meta:
        verbose_name = 'Copil'
        verbose_name_plural = 'Copii'

    def __str__(self):
        return f'{self.nume} {self.prenume}'

class Plati(models.Model):
    copil = models.ForeignKey(Copil, on_delete = models.SET_NULL, null = True)
    data_creare = models.DateTimeField(auto_now_add = True)

    class Meta:
        verbose_name = 'Plata'
        verbose_name_plural = 'Plati'

    def __str__(self):
        return f'{self.copil} - {self.data_creare}'
