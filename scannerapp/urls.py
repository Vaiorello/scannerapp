from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register('licee', views.LiceuView, basename='LiceuRest')
router.register('copii', views.CopilView, basename='CopilRest')
router.register('plati', views.PlatiView, basename='PlatiRest')

urlpatterns = [
    path('api/', include(router.urls)),
    path('', views.index, name = 'scanner-index'),
    path('mobile/', views.mobile, name = 'scanner-mobile'),
    path('add/<str:nume>/<str:prenume>/<str:serie>/<str:cnp>/', views.add, name = 'scannerapp-add'),
    path('add/', views.add, name = 'scannerapp-add-gol')
]