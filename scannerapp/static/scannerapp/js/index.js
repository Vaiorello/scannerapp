$(function () {
    var copilTable = $('#dataTableCopil').DataTable({
        "serverSide": true,
        "ajax": {
            "url": "/api/copii/?format=datatables",

            "data": function (d) {
               
            }
        },

        "buttons": [],
        "order": [[0, "desc"]],
        "columns": [
            {

                "data": 'id', "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                    $(nTd).html('<a href="/transport-details/' + oData.id + '">' + oData.id + '</a>');
                }, "width": "1%"

            },
            { "data": "nume"},
            { "data": "prenume" },
            { "data": "cnp" },
            { "data": "telefon" },
            { "data": "serie_buletin" },
            { "data": "liceu.denumire" },
        ],
    });

});
$(function () {
    var plataTable = $('#dataTablePlata').DataTable({
        "serverSide": true,
        "ajax": {
            "url": "/api/plati/?format=datatables",

            "data": function (d) {
               
            }
        },

        "buttons": [],
        "order": [[0, "desc"]],
        "columns": [
            {

                "data": 'id', "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                    $(nTd).html('<a href="/transport-details/' + oData.id + '">' + oData.id + '</a>');
                }, "width": "1%"

            },
            {"data": 'copil.nume'},
            {"data": 'copil.prenume'},
            {"data": 'copil.telefon'},
            {"data": 'copil.cnp'},
            {"data": 'data_creare', "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                let date = new Date(Date.parse(oData.data_creare)).toLocaleDateString('en-GB');
                let time = new Date(Date.parse(oData.data_creare)).toLocaleTimeString('en-GB');
                $(nTd).html(date + ' - ' + time);
              }, }
        ],
    });

});